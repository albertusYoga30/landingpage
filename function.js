function select(link) {
  const item = link.parentNode;
  const tabs = link.parentNode;
  const index = Array.prototype.indexOf.call(tabs.children, item);
  const items = tabs.querySelectorAll(".tab-item");

  tabs.style.setProperty("--index", index + 1);
  items.forEach((items) => items.classList.remove("active"));
  item.classList.add("active");
}
