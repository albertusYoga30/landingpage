class projects {
  constructor(judul, gambar, deskripsi, link) {
    this.judul = judul;
    this.gambar = gambar;
    this.deskripsi = deskripsi;
    this.link = link;
  }
}

var item1 = new projects(
  "Webstise",
  "Image/p2.jpg",
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit pariatur iusto debitis recusandae! Commodi quos culpa ipsam fugiat labore hic eos doloribus nemo dolore obcaecati, quam voluptate ad aut soluta.",
  "#home"
);
var item2 = new projects(
  "Ai",
  "Image/p2.jpg",
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit pariatur iusto debitis recusandae! Commodi quos culpa ipsam fugiat labore hic eos doloribus nemo dolore obcaecati, quam voluptate ad aut soluta.",
  "#home"
);
var item3 = new projects(
  "Mobile",
  "Image/p2.jpg",
  "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit pariatur iusto debitis recusandae! Commodi quos culpa ipsam fugiat labore hic eos doloribus nemo dolore obcaecati, quam voluptate ad aut soluta.",
  "#home"
);

var itemList = [item1, item2, item3];

$(document).ready(function () {
  $.each(itemList, function (i) {
    var temp = `
    <div class="col-md-4">
        <div class="project-list">
        <h2>${itemList[i].judul}</h2>
        <img src="${itemList[i].gambar}" class="card-img-top" alt="...">
        <div class="project-des mt-1 mb-1">
            <p>${itemList[i].deskripsi}</p>
            </div>
            <div class="seeMore">
            <button id="btn-seeMore" class="btn btn-lg" onclick="location.href= '${itemList[i].link}'" type="button">See more</button>
            </div>
            </div>
            </div>`;
    $("#project-data").append(temp);
  });
});
